<!DOCTYPE html>
<html lang="ca">
 <head>
  <meta charset="uft-8">
  <title>Ejer1PHP</title>
 </head>
 <body>
  <?php
   echo "<h1>Ejercicio 1</h1>";
   echo "<h2>Array estandard</h3>";
   $varArray1 = array('Elem1','Elem2','Elem3','Elem4');
   echo "<p>" . $varArray1[0] . "</p>";
   echo "<p>" . $varArray1[1] . "</p>";
   echo "<p>" . $varArray1[2] . "</p>";
   echo "<p>" . $varArray1[3] . "</p>";
   echo "<p><pre>";
   print_r($varArray1);
   echo "</pre></p>";
   echo "<h2>Array associatiu</h3>";

   $varArray2 = array(
	'clau1'=>'Elem1',
	'clau2'=>'Elem2',
	'clau3'=>'Elem3',
	);
   echo "<p>" . $varArray2[clau1] . "</p>";
   echo "<p>" . $varArray2[clau2] . "</p>";
   echo "<p>" . $varArray2[clau3] . "</p>";
   echo "<p><pre>";
   print_r($varArray2);
   echo "</pre></p>";

   echo "<h2>ArrayKeys</h3>";
   $varArray3=array(
	"uno",
	"uno",
	"dos",
	"tres",
	"uno");
   echo "<p><pre>";
   print_r($varArray3);
   echo "</pre></p>";
   echo "<p>Nos dira las llaves que sean 'uno'</p>";
   print_r(array_keys($varArray3,"uno"));

  ?>
 </body>
</html>

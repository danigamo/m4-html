<!DOCTYPE html>
<html lang="ca">
 <head>
  <meta charset="uft-8">
  <title>Ejer2PHP</title>
 </head>
 <body>
  <?php
   echo "<h1>Ejercicio 2</h1>";
   $var1 = "3";
   $var2 = "9";
   echo "<h3>Funciones Matemáticas</h3>";
   echo "<p>Raiz Cuadrada de " . $var2 . " es: " . sqrt($var2) . "</p>";
   echo "<p>Valor mas alto entre $var1 y $var2 es: " .  max($var1, $var2);
   echo "<p>Valor mas bajo entre $var1 y $var2 es: " .  min($var1, $var2);
   echo "<p> Convertir el valor $var1 a binario: " . decbin($var1);
  ?>
 </body>
</html>
